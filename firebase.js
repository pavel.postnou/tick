import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore/";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDa_e8_WnomJqUTlBg3OZ9h2Eg7UEHw6ck",
  authDomain: "mytick-107c3.firebaseapp.com",
  projectId: "mytick-107c3",
  storageBucket: "gs://mytick-107c3.appspot.com/",
  messagingSenderId: "783352415712",
  appId: "1:783352415712:web:9d46be61ca5cd5d3aad785",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const auth = getAuth();

export { auth, app, db, firebaseConfig };
