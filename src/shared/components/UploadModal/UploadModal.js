import React, { useState } from "react";
import {
  View,
  Text,
  Modal,
  TouchableHighlight,
  ActivityIndicator,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { db } from "../../../../firebase";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { setDoc, doc } from "firebase/firestore/";
import { Video } from "expo-av";
import { styles } from "./UploadModel.style";

export default function UploadModal(props) {
  const { email, modalVisible, setModalVisible } = props;
  const [video, setVideo] = useState(null);
  const [uploading, setUploading] = useState(false);
  const storage = getStorage();

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: true,
      aspect: [16, 9],
      videoMaxDuration: 60,
      VideoExportPreset: 4,
      quality: 0,
    });
    if (!result.cancelled) {
      setVideo(result.uri);
    }
  };

  const uploadVideo = async () => {
    if (video) {
      setUploading(true);
      const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function () {
          resolve(xhr.response);
        };
        xhr.onerror = function (e) {
          console.log(e);
          reject(new TypeError("Network request failed"));
        };
        xhr.responseType = "blob";
        xhr.open("GET", video, true);
        xhr.send(null);
      });

      const refs = ref(storage, new Date().toISOString());
      uploadBytes(refs, blob).then((snapshot) => {
        getDownloadURL(snapshot.ref).then(async (downloadURL) => {
          const id = downloadURL.slice(-11, -1);
          await setDoc(doc(db, email, id), {
            email: email,
            uri: downloadURL,
            comments: [],
            likes: [],
            fileName: snapshot.metadata.name,
          });
          setUploading(false);
          setModalVisible(false);
          setVideo(null);
        });
      });
    } else alert("choose video");
  };

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <View style={styles.centeredView}>
        {uploading ? (
          <View style={styles.activityIndicator}>
            <ActivityIndicator size={70} color="#0000ff" />
          </View>
        ) : (
          <View style={styles.modalView}>
            {video ? (
              <Video source={{ uri: video }} style={styles.video} />
            ) : null}
            <TouchableHighlight
              style={styles.modalUpload}
              onPress={() => {
                pickImage();
              }}
            >
              <Text style={styles.text}>choose video</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.modalUpload}
              onPress={() => {
                uploadVideo();
              }}
            >
              <Text style={styles.text}>upload</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.modalUpload}
              onPress={() => {
                setModalVisible(false), setVideo(null);
              }}
            >
              <Text style={styles.text}>close</Text>
            </TouchableHighlight>
          </View>
        )}
      </View>
    </Modal>
  );
}
