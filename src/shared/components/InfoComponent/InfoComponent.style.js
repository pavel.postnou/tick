import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  info: {
    position: "absolute",
    top: 0,
    alignSelf: "center",
    backgroundColor: "rgba(166, 166, 166, 0.4)",
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
    borderColor: "darkgrey",
    borderWidth: 1,
  },
  infoText: {
    color: "#fa695f",
    fontWeight: "bold",
    fontSize: 20,
  },
});
