import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { db } from "../../../../firebase";
import { collection, query, onSnapshot } from "firebase/firestore/";
import { styles } from "./InfoComponent.style";

function InfoComponent(props) {
  const { email, videoArray } = props;
  const [newArrLength, setNewArrLength] = useState(videoArray.length);
  const [isNewVideo, setIsNewVideo] = useState(false);
  const q = query(collection(db, email));

  useEffect(() => {
    onSnapshot(q, (snapshot) => {
      const arr = [];
      snapshot.forEach((doc) => {
        arr.push(doc.data());
      });
      arr.length > newArrLength
        ? (setNewArrLength(arr.length), setIsNewVideo(true))
        : setIsNewVideo(false);
    });
  }, [videoArray]);

  return isNewVideo ? (
    <View style={styles.info}>
      <Text style={styles.infoText}>New video on refresh</Text>
    </View>
  ) : null;
}

export default React.memo(InfoComponent);

