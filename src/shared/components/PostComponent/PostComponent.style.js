import { StyleSheet, Dimensions } from "react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export const styles = StyleSheet.create({
  trash: {
    position: "absolute",
    top: 30,
    right: 5,
  },
  comment: {
    position: "absolute",
    top: 70,
    right: 5,
  },
  share: {
    position: "absolute",
    top: 110,
    right: 5,
  },
  like: {
    position: "absolute",
    top: 150,
    right: 5,
  },
  text: {
    position: "absolute",
    top: 175,
    right: 12,
    color: "red",
    fontWeight: "bold",
  },
  videoContainer: {
    flex: 1,
    borderColor: "white",
    borderBottomWidth: 1,
    backgroundColor: "black",
  },
  video: {
    alignSelf: "center",
    width: width,
    height: height,
  },
  errorText: {
    width: width,
    height: height,
    color: "white",
  },
});
