import React, { useEffect, useState } from "react";
import { View, Text, Share } from "react-native";
import { Icon } from "react-native-elements";
import { db } from "../../../../firebase";
import { doc, updateDoc, onSnapshot, deleteDoc } from "firebase/firestore/";
import { getStorage, ref, deleteObject } from "firebase/storage";
import { styles } from "./PostComponent.style";
import VideoPlayer from "expo-video-player";
import CommentsModal from "../CommentsModal";

function PostComponent(props) {
  const { email, item } = props;
  const [modalComVisible, setModalComVisible] = useState(false);
  const [comments, setComments] = useState([]);
  const [id, setId] = useState("");
  const [url, setUrl] = useState(null);
  const [text, setText] = useState("video");
  const [itemTrack, setItemTrack] = useState(item);
  const storage = getStorage();
  const videoProps = {
    shouldPlay: false,
    resizeMode: "contain",
    defaultControlsVisible: false,
    timeVisible: false,
  };

  useEffect(async () => {
    onSnapshot(doc(db, email, item.docId), (doc) => {
      if (doc.data()) {
        const Obj = {
          docId: doc.id,
          uri: doc.data().uri,
          likes: doc.data().likes,
          fileName: doc.data().fileName,
        };
        setComments(doc.data().comments);
        setItemTrack(Obj);
      } else {
        setComments(null);
        setItemTrack(null);
      }
    });
  }, []);

  const shareVideo = (uri) => {
    Share.share({
      title: "hi",
      message: uri,
    });
  };

  const addLikes = async (likes, docId) => {
    if (likes.indexOf(email) != -1) {
      likes.splice(likes.indexOf(email), 1);
      const myDoc = doc(db, email, docId);
      await updateDoc(myDoc, {
        likes: likes,
      });
    } else {
      likes.push(email);
      const myDoc = doc(db, email, docId);
      await updateDoc(myDoc, {
        likes: likes,
      });
    }
  };

  const removePost = async () => {
    await deleteDoc(doc(db, email, itemTrack.docId));
    const postRef = ref(storage, itemTrack.fileName);
    deleteObject(postRef)
      .then(() => {
        alert("file deleted");
      })
      .catch((error) => {
        alert(error);
      });
  };

  useEffect(async () => {
    if (itemTrack) {
      const response = await fetch(itemTrack.uri);
      response.status == "200"
        ? setUrl(itemTrack.uri)
        : setText((await response.json()).error.message);
    }
  }, [itemTrack]);

  return (
    <View>
      {itemTrack ? (
        <View style={styles.videoContainer}>
          {url ? (
            <VideoPlayer
              videoProps={{
                ...videoProps,
                source: {
                  uri: url,
                },
              }}
              defaultControlsVisible={false}
              style={styles.video}
            />
          ) : (
            <Text style={styles.errorText}>{text}</Text>
          )}
          <View style={styles.trash}>
            <Icon
              name="trash-o"
              type="font-awesome"
              color="white"
              onPress={() => removePost()}
            />
          </View>
          <View style={styles.comment}>
            <Icon
              name="commenting-o"
              type="font-awesome"
              color="white"
              onPress={() => setModalComVisible(true)}
            />
          </View>
          <View style={styles.share}>
            <Icon
              name="share-alt"
              type="font-awesome"
              color="white"
              onPress={() => shareVideo(itemTrack.uri)}
            />
          </View>
          <View style={styles.like}>
            <Icon
              name="heart"
              type="font-awesome"
              color="white"
              onPress={() => addLikes(itemTrack.likes, itemTrack.docId)}
            />
          </View>
          <Text style={styles.text}>{itemTrack.likes.length}</Text>
          <CommentsModal
            comments={comments}
            docId={itemTrack.docId}
            modalComVisible={modalComVisible}
            setModalComVisible={setModalComVisible}
            email={email}
          />
        </View>
      ) : null}
    </View>
  );
}

export default React.memo(PostComponent);
