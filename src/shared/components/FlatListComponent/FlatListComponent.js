import React, { useState, useEffect } from "react";
import { FlatList } from "react-native";
import PostComponent from "../PostComponent";

function FlatListComponent(props) {
  const { videoArray, email, setRefresh, refresh } = props;
  const [offset, setOffset] = useState(3);
  const [limit, setLimit] = useState(5);
  const [res, setRes] = useState([]);

  useEffect(() => {
    setOffset(2);
    setLimit(4);
    setRes(videoArray.slice(0, 2));
  }, [videoArray]);

  const renderItem = ({ item }) => {
    return <PostComponent item={item} email={email} />;
  };

  const fetchResult = () => {
    videoArray.length >= offset
      ? (setRes(res.concat(videoArray.slice(offset, limit))),
        setOffset(limit),
        setLimit(limit + 2))
      : null;
  };

  return (
    <FlatList
      data={res}
      renderItem={renderItem}
      keyExtractor={(item, index) => index}
      onEndReached={() => fetchResult()}
      onEndReachedThreshold={0}
      refreshing={false}
      onRefresh={() => setRefresh(!refresh)}
    />
  );
}

export default React.memo(FlatListComponent);
