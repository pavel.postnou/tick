import React, { useState, useCallback, useMemo } from "react";
import {
  View,
  Text,
  Modal,
  TouchableHighlight,
  TextInput,
  ScrollView,
} from "react-native";
import { styles } from "./CommentsModal.style";
import { db } from "../../../../firebase";
import { doc, updateDoc } from "firebase/firestore/";

export default function CommentsModal(props) {
  const [text, setText] = useState("");
  const { comments, email, docId, modalComVisible, setModalComVisible } = props;

  const addComments = useCallback(async () => {
    if (text.length != 0) {
    const arr = comments;
    arr.push(text);
    try {
      const myDoc = doc(db, email, docId);
      await updateDoc(myDoc, {
        comments: arr,
      });
      setText("");
    } catch (e) {
      alert("An error occurred: " + e);
    }
  }
  else alert ("add some text")
  }, [text]);

  const commentsView = useMemo(() => {
    return comments && comments.length > 0
      ? comments.map((item, index) => (
          <View key={index} style={styles.comments}>
            <ScrollView keyboardShouldPersistTaps="handled">
              <Text style={styles.commentsHeader}>{email}</Text>
              <Text style={styles.commentsText}>{item}</Text>
            </ScrollView>
          </View>
        ))
      : null;
  }, [comments]);

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Modal animationType="fade" transparent={true} visible={modalComVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>{commentsView}</View>
          <TextInput
            style={styles.textInput}
            value={text}
            placeholder="comment"
            onChangeText={(text) => setText(text)}
          />
          <TouchableHighlight style={styles.modalUpload} onPress={addComments}>
            <Text style={styles.text}>Add comment</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.modalUpload}
            onPress={() => (setModalComVisible(false), setText(""))}
          >
            <Text style={styles.text}>Close</Text>
          </TouchableHighlight>
        </View>
      </Modal>
    </ScrollView>
  );
}
