import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10,
    },
    modalView: {
      width: "90%",
      height: "70%",
      margin: 10,
      backgroundColor: "white",
      borderRadius: 10,
      padding: 10,
      alignItems: "center",
      shadowColor: "#000",
      justifyContent: "space-evenly",
    },
    modalUpload: {
      alignSelf: "center",
      backgroundColor: "#2196F3",
      padding: 5,
      borderRadius: 30,
      width: "95%",
      marginBottom: 10,
      alignItems: "center",
    },
    textInput: {
      backgroundColor: "white",
      width: "50%",
      borderColor: "blue",
      borderWidth: 1,
      borderRadius: 10,
      marginBottom: 10,
    },
    comments: {
      backgroundColor: "lightgrey",
      width: "95%",
      paddingHorizontal: 10,
      borderColor: "grey",
      borderWidth: 2,
      borderRadius: 10,
      marginBottom: 10,
    },
    commentsText: {
      fontWeight: "bold",
      color: "blue",
    },
    commentsHeader: {
      fontWeight: "bold",
      color: "green",
    },
  });