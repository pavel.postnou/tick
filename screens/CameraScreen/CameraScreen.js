import React, { useState, useEffect } from "react";
import { View, Platform, Text, StatusBar } from "react-native";
import * as ImagePicker from "expo-image-picker";
import firebase from "firebase/compat/app";
import { collection, query, getDocs } from "firebase/firestore/";
import { db, firebaseConfig } from "../../firebase";
import { Icon } from "react-native-elements";
import UploadModal from "../../src/shared/components/UploadModal";
import { styles } from "./CamersScreen.style";
import FlatListComponent from "../../src/shared/components/FlatListComponent";
import InfoComponent from "../../src/shared/components/InfoComponent";
import { useNavigation } from "@react-navigation/core";

export default function CameraScreen(props) {
  const email = props.route.params.result.user.email;
  const [videoArray, setVideoArray] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [refresh, setRefresh] = useState(true);
  const navigation = useNavigation();

  const q = query(collection(db, email));

  useEffect(async () => {
    const arr = [];
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      arr.push({
        docId: doc.id,
        uri: doc.data().uri,
        likes: doc.data().likes,
      });
    });
    arr.length != videoArray.length ? setVideoArray(arr) : null;
  }, [refresh]);

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, access denied");
        }
      }
    })();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar hidden={true} />

      <View style={styles.container}>
        <FlatListComponent
          videoArray={videoArray}
          email={email}
          setRefresh={setRefresh}
          refresh={refresh}
        />
      </View>
      <View style={styles.add}>
        <Icon
          raised
          name="plus"
          type="font-awesome"
          color="grey"
          onPress={() => {
            setModalVisible(true);
          }}
          size={15}
        />
        <Icon
          raised
          name="refresh"
          type="font-awesome"
          color="grey"
          onPress={() => {
            setRefresh(!refresh);
          }}
          size={15}
        />
        <Icon
          raised
          name="sign-out"
          type="font-awesome"
          color="grey"
          onPress={() => {
            navigation.replace("Login");
          }}
          size={15}
        />
      </View>
      {videoArray.length > 0 ? (
        <InfoComponent videoArray={videoArray} email={email} />
      ) : null}
      <UploadModal
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        email={email}
      />
    </View>
  );
}
