import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  video: {
    alignSelf: "center",
    width: width,
    height: height,
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  modalTouch: {
    position: "absolute",
    bottom: 10,
    alignSelf: "center",
    backgroundColor: "#308aff",
    padding: 5,
    borderRadius: 30,
    width: "95%",
    marginBottom: 10,
    alignItems: "center",
  },
  add: {
    position: "absolute",
    bottom: 20,
    alignSelf: "center",
    flexDirection:"row"
  },
  info: {
    position: "absolute",
    top: 0,
    alignSelf: "center",
    backgroundColor:'rgba(166, 166, 166, 0.4)',
    paddingHorizontal:20,
    paddingVertical:5,
    borderRadius:10,
    borderColor:"darkgrey",
    borderWidth:1
  },
  infoText: {
    color: "#fa695f",
    fontWeight: "bold",
    fontSize: 20,
  },
});
